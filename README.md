# aaaa

Aaaa front-end.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Have node installed.

### Installing

Install the Expo CLI globaly.

```
$ npm install -g expo-cli
```

Install the project's modules.

```
$ npm install
```

Launch the app.

```
$ npm start
```

### Testing

Test suite uses the X module.

```
```