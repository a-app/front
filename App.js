import React from 'react';
import { Provider } from 'react-redux';

import Home from './components/home.js';
import { store } from './state/store.js';
import { retrieveCharacter } from './state/requests.js';


export default class App extends React.Component {
  render() {
    retrieveCharacter();
    return (
      <Provider store={store}>
        <Home />
      </Provider>
    );
  }
}

