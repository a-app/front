import { StyleSheet } from 'react-native';

export const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    create: {
        flex: 1,
        backgroundColor: '#3F00FF',
        alignItems: 'center',
        justifyContent: 'center',
    }
});
