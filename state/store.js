import { createStore } from 'redux';

export const CharacterType = {
    LOADING: ' LOADING',
    FOUND: ' FOUND',
    NOT_FOUND: ' NOT_FOUND',
}

export const EncounterType = {
    NONE: 'NONE',
    WAIT: 'WAIT',
    ANSWER: 'ANSWER',
    DONE: 'DONE',
}

export const initialState = {
    characterState: CharacterType.LOADING,
    characterId: null,
    character: {
        name: '',
        sentence: '',
    },
    encounter: {
        state: EncounterType.NONE,
        name: '',
        sentence: '',
        self: false,
        other: false
    },
}

function reduce(state = initialState, action) {
    switch (action.type) {
        case CharacterType.FOUND:
            return Object.assign({}, state, {
                characterState: CharacterType.FOUND,
                characterId: action.id,
                character: action.character,
            })
        case CharacterType.NOT_FOUND:
            return Object.assign({}, state, {
                characterState: CharacterType.NOT_FOUND,
            })
        case CharacterType.LOADING:
            return Object.assign({}, state, {
                characterState: CharacterType.LOADING,
            })
        case EncounterType.NONE:
            return Object.assign({}, state, {
                encounter: {
                    state: EncounterType.NONE,
                }
            })
        case EncounterType.WAIT:
            return Object.assign({}, state, {
                encounter: {
                    state: EncounterType.WAIT,
                }
            })
        case EncounterType.ANSWER:
            return Object.assign({}, state, {
                encounter: {
                    state: EncounterType.ANSWER,
                    name: action.name,
                    sentence: action.sentence,
                }
            })
        case EncounterType.DONE:
            return Object.assign({}, state, {
                encounter: {
                    state: EncounterType.DONE,
                    self: action.self,
                    other: action.other,
                }
            })
        default:
            return state;
    }
}

export let store = createStore(reduce);


export function character_found(id, character) {
    store.dispatch({ type: CharacterType.FOUND, id, character })
}

export function character_not_found() {
    store.dispatch({ type: CharacterType.NOT_FOUND })
}

export function encounter_none() {
    store.dispatch({ type: EncounterType.NONE })
}

export function encounter_wait() {
    store.dispatch({ type: EncounterType.WAIT })
}

export function encounter_answer(name, sentence) {
    store.dispatch({ type: EncounterType.ANSWER, name, sentence })
}

export function encounter_done(self, other) {
    store.dispatch({ type: EncounterType.DONE, self, other })
}