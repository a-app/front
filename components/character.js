import React, { Component } from 'react';
import { Button, Text, View, TouchableOpacity, Image } from 'react-native';
import { StyleSheet } from 'react-native';

import { EncounterType } from '../state/store.js';
import { characterWS } from '../state/requests.js';

const EncounterNone = ({ send }) => (
    <View>
        <TouchableOpacity onPress={() => send('A')}>
            <Image source={require('../img/a.png')} style={{ width: 96, height: 96 }} />
        </TouchableOpacity>
    </View>
)

const EncounterWait = ({ send }) => (
    <View>
        <Text>Looking for an encounter ...</Text>
        <Text>Press B to cancel</Text>
        <Button
            onPress={() => send('B')}
            title='B'
        />
    </View>
)

const EncounterAnswer = ({ send, encounter }) => (
    <View>
        <Text>Encounter found with :</Text>
        <Text>{encounter.name}</Text>
        <Text>{encounter.sentence}</Text>
        <Button
            onPress={() => send('accept')}
            title='accept'
        />
        <Button
            onPress={() => send('refuse')}
            title='refuse'
        />
    </View>)

const EncounterDone = ({ self, other }) => {
    const selfTxt = self ? 'You accepted the encounter.' : 'You refused the encounter.';
    const otherTxt = other ? 'You were accepted.' : 'You were refused.';
    const txt = self && other ? 'Encounter successful.' : 'Encounter unsuccessful.';
    return (
        <View>
            <Text>{selfTxt}</Text>
            <Text>{otherTxt}</Text>
            <Text>{txt}</Text>
        </View>
    );
}

class Encounter extends Component {
    render() {
        switch (this.props.encounter.state) {
            case EncounterType.NONE:
                return (<EncounterNone send={this.props.send} />);
            case EncounterType.WAIT:
                return (<EncounterWait send={this.props.send} />);
            case EncounterType.ANSWER:
                return (
                    <EncounterAnswer
                        send={this.props.send}
                        encounter={this.props.encounter}
                    />
                );
            case EncounterType.DONE:
                return (
                    <EncounterDone
                        self={this.props.encounter.self}
                        other={this.props.encounter.other}
                    />
                );
        }
    }
}

export class CharacterScreen extends Component {
    state = characterWS(this.props.id);
    render() {
        return (
            <View style={style.container}>
                <View style={style.boxContainer}>
                    <View style={style.box}>
                        <Text style={{ fontSize: 18 }}>
                            {this.props.character.name}: {this.props.character.sentence}
                        </Text>
                    </View>
                </View>
                <View style={style.encounterContainer}>
                    <Encounter
                        send={this.state.send}
                        encounter={this.props.encounter}
                    />
                </View>
                <View style={style.footerContainer}>
                    <Text>Footer</Text>
                </View>
            </View>
        );
    }
}

export const style = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    boxContainer: {
        flex: 2,
        width: '100%',
        paddingHorizontal: '5%',
        justifyContent: 'center',
    },
    encounterContainer: {
        flex: 3,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    footerContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    box: {
        padding: 4,
        borderWidth: 1,
        borderColor: '#1f1f1f',
    },
});