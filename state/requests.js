import { AsyncStorage } from 'react-native';

import { character_found, character_not_found, encounter_wait, encounter_done, encounter_answer, encounter_none } from './store.js';

const KEY = 'a:character:id';
const URL = '192.168.43.214';
const API = 'http://' + URL + ':8000/';
const WS = 'ws://' + URL + ':8000/';

const createUrl = () => API + 'characters/';
const detailUrl = id => API + 'characters/' + id + '/';
const characterUrl = id => WS + 'ws/characters/' + id + '/';

export async function retrieveCharacter() {
    try {
        const id = await AsyncStorage.getItem(KEY);
        if (id === null) {
            character_not_found();
        } else {
            const url = detailUrl(id);
            try {
                const response = await fetch(url);
                if (response.status === 200) {
                    const responseJson = await response.json();
                    character_found(id, responseJson);
                } else {
                    //TODO: former character deleted ?
                    character_not_found();
                }
            } catch (error) {
                console.error('DEBUG FetchDetail', url, error);
            }
        }
    } catch (error) {
        console.error('DEBUG AsyncStorage', error);
    }
}

export async function createCharacter(name, sentence) {
    const character = { name, sentence };
    try {
        const response = await fetch(createUrl(), {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(character),
        });
        if (response.status === 201) {
            const responseJson = await response.json();
            const id = responseJson.uuid;
            try {
                await AsyncStorage.setItem(KEY, id);
                character_found(id, character);
            } catch (error) {
                console.error('DEBUG AsyncStorage', error);
            }
        }
    } catch (error) {
        console.error('DEBUG FetchCreate', createUrl(), error)
    }
}

export async function resetCharacter() {
    try {
        const response = await AsyncStorage.removeItem(KEY);
    } catch (error) {
        console.error('DEBUG AsyncStorage', error);
    }
}

const WebSocketSend = (ws, cmd, data = {}) => {
    const payload = Object.assign(data, { cmd });
    ws.send(JSON.stringify(payload));
}

const RecoWebSocket = (url, onMessage) => {
    //const rws = new ReconnectingWebSocket(ws_scheme + '://' + url);
    const rws = new WebSocket(url);
    const send = WebSocketSend.bind(WebSocketSend, rws);
    rws.onopen = (e) => {
        console.log(url + ' socket opened');
    }
    rws.onclose = (e) => {
        console.warn(url + ' socket closed');
    }
    rws.onmessage = (e) => {
        console.log(url + ' socket received');
        onMessage(JSON.parse(e.data));
    }
    rws.onerror = (e) => {
        console.error(url + ' socket failed', e);
    }
    return { rws, send };
}

const characterOnMessage = (data) => {
    if (data.wait) {
        encounter_wait();
    } else if (data.encounter) {
        const { name, sentence } = data.encounter;
        encounter_answer(name, sentence);
    } else if (data.response) {
        const { self, other } = data.response;
        encounter_done(self, other);
    } else {
    }
}

export const characterWS = (id) => {
    return RecoWebSocket(characterUrl(id), characterOnMessage);
}