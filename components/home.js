import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { Constants } from 'expo';

import { CharacterType } from '../state/store.js';
import { LoadScreen } from './load.js';
import { CreateScreen } from './create.js';
import { CharacterScreen } from './character.js';

const mapStateToProps = state => ({
    characterState: state.characterState,
    characterId: state.characterId,
    character: state.character,
    encounter: state.encounter,
})

const mapDispatchToProps = dispatch => ({})

class Home extends Component {
    state = {}

    screen = () => {
        switch (this.props.characterState) {
            case CharacterType.LOADING:
                return (<LoadScreen />);
            case CharacterType.FOUND:
                return (
                    <CharacterScreen
                        character={this.props.character}
                        id={this.props.characterId}
                        encounter={this.props.encounter}
                    />
                );
            case CharacterType.NOT_FOUND:
                return (<CreateScreen />);
        }
    }

    render() {
        return (
            <View style={style.home}>
                {this.screen()}
            </View>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)

const style = StyleSheet.create({
    home: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        marginTop: Constants.statusBarHeight,
    },
});