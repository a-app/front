import React, { Component } from 'react';
import { Text, View, KeyboardAvoidingView } from 'react-native';
import { Button, Icon, Input } from 'react-native-elements';
import { StyleSheet } from 'react-native';

import { createCharacter } from '../state/requests.js';

export class CreateScreen extends Component {
    state = {
        name: '',
        sentence: '',
        nameValid: true,
        sentenceValid: true,
    }

    validateName = () => {
        const name = this.state.name.trim();
        const nameValid = name.length > 0;
        this.setState({ name, nameValid });
        nameValid || this.nameInput.shake();
    }

    validateSentence = () => {
        const sentence = this.state.sentence.trim();
        const sentenceValid = sentence.length > 0;
        this.setState({ sentence, sentenceValid });
        sentenceValid || this.sentenceInput.shake();
    }

    sendForm = () => {
        this.validateName();
        this.validateSentence();
        if (this.state.nameValid && this.state.sentenceValid) {
            createCharacter(this.state.name, this.state.sentence);
        }
    }

    render() {
        const nameErr = this.state.nameValid ? null : 'Your name is too short.';
        const sentenceErr = this.state.sentenceValid ? null : 'Your sentence is too short.';
        return (
            <KeyboardAvoidingView style={style.container} behavior="padding">
                <Text style={style.title}>Create your Character</Text>
                <View style={style.form}>
                    <Input
                        placeholder='Name'
                        returnKeyType='next'
                        value={this.state.name}
                        errorMessage={nameErr}
                        ref={input => (this.nameInput = input)}
                        blurOnSubmit={false}
                        onChangeText={name => this.setState({ name })}
                        onSubmitEditing={() => {
                            this.validateName();
                            this.sentenceInput.focus();
                        }}
                        leftIcon={
                            <Icon
                                name='user'
                                type='feather'
                            />
                        }
                        inputStyle={style.inputStyle}
                        errorStyle={style.inputError}
                        inputContainerStyle={style.inputContainer}
                    />
                    <Input
                        placeholder='Sentence'
                        returnKeyType='send'
                        errorMessage={sentenceErr}
                        value={this.state.sentence}
                        ref={input => (this.sentenceInput = input)}
                        blurOnSubmit={false}
                        onChangeText={sentence => this.setState({ sentence })}
                        onSubmitEditing={this.sendForm}
                        leftIcon={
                            <Icon
                                name='message-circle'
                                type='feather'
                            />
                        }
                        inputStyle={style.inputStyle}
                        errorStyle={style.inputError}
                        inputContainerStyle={style.inputContainer}
                    />
                </View>
                <Button
                    title='START'
                    onPress={this.sendForm}
                    buttonStyle={style.button}
                    containerStyle={{ width: '80%' }}
                />
            </KeyboardAvoidingView>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 28,
        fontWeight: 'bold',
    },
    form: {
        width: '80%',
        height: '60%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputStyle: {
        flex: 1,
        fontSize: 18,
        marginLeft: 16,
        color: '#1f1f1f',
    },
    inputError: {
        marginTop: 0,
        textAlign: 'center',
        color: '#cc0000',
    },
    inputContainer: {
        height: 50,
        marginVertical: 10,
        paddingLeft: 4,
        paddingRight: 8,
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#1f1f1f',
    },
    button: {
        height: 50,
        marginLeft: 12,
        marginRight: 12,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1f1f1f',
    },
});