import React, { Component } from 'react';
import { Text, View } from 'react-native';

import { style } from './style.js';

export class LoadScreen extends Component {
    render() {
        return (
            <View style={style.container}>
                <Text>load_character</Text>
            </View>
        );
    }
}